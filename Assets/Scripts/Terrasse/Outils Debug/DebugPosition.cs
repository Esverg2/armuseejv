﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

// OUTIL DE DEBUG PERMETTANT DE DEPLACER LES OBJETS DE LA SCENE AR GRACE UN INTERFACE DEBUG
// OUTIL DE DEVELOPPEMENT, NE SERA PAS DANS LA VERSION FINALE !
public class DebugPosition : MonoBehaviour
{
    [Space(10), Header("Liste des objets déplaçables"), Space(20)]
    [Header("Outil de debug permettant de déplacer les objets comme sur l'éditeur mais en PlayMode"), Header("Ne pas oublier de mettre vos objets déplaçables dans le tableau (AllObjects)")]
    
    [Tooltip("Liste des objets déplaçables")]
    public GameObject[] allObjects;

    [Space(10), Header("Ne plus toucher ensuite"), Space(30)]
    [Tooltip("POSITION - Zones d'entrées des valeurs XYZ"), Space(10)]
    public InputField[] PositionInputs;
    [Tooltip("ROTATION - Zones d'entrées des valeurs XYZ"), Space(10)]
    public InputField[] RotationInputs;
    [Tooltip("SCALE - Zones d'entrées des valeurs XYZ"), Space(10)]
    public InputField[] ScaleInputs;
    [Tooltip("SCALE ALL - Choix multiply ou divide"), Space(10)]
    public Toggle[] toggleScale;
    [Tooltip("Nom de l'objet utilisé"), Space(10)]
    public Text textObjName;

    [Header("Booléen, objet utilisé")]
    [Tooltip("L'objet est il utilisé ?"), Space(10)]
    public List<bool> isObjectUsed;

    [Tooltip("Text Modif en cours"), Space(10)]
    public Text optionsText;
    private int _optionsChange = -1;

    void Start()
    {
        // Création du Booléenpour chaque objet déplaçable
        foreach (GameObject allObject in allObjects)
        {
            isObjectUsed.Add(new bool());
        }

        ChangeOptions();
    }

    void Update()
    {
        // Clic Objet
        if (Input.GetMouseButtonDown(0))
        {
            CastRay();
        }
    }

    // Fonction au clic sur un objet
    void CastRay()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100))
        {
            // Au moment d'un clic sur un objet
            // Désactivation ou activation de la modification de chaque objets
            for (int i = 0; i < allObjects.Length; i++)
            {
                if(hit.transform.gameObject == allObjects[i])
                {
                    isObjectUsed[i] = true;
                }
                else
                {
                    isObjectUsed[i] = false;
                }              
            }

            // Affichage du nom de l'objet choisi
            textObjName.text = hit.transform.gameObject.name;

            // AFFICHAGE VALEURS

            // Affichage de la POSITION de l'objet choisi
            PositionInputs[0].text = hit.transform.position.x.ToString();
            PositionInputs[1].text = hit.transform.position.y.ToString();
            PositionInputs[2].text = hit.transform.position.z.ToString();

            RotationInputs[0].text = hit.transform.rotation.x.ToString();
            RotationInputs[1].text = hit.transform.rotation.y.ToString();
            RotationInputs[2].text = hit.transform.rotation.z.ToString();

            ScaleInputs[0].text = hit.transform.localScale.x.ToString();
            ScaleInputs[1].text = hit.transform.localScale.y.ToString();
            ScaleInputs[2].text = hit.transform.localScale.z.ToString();
        }      
    }

    public void ChangeOptions()
    {
        if (_optionsChange < 2)
        {
            _optionsChange++;
        }
        else
        {
            _optionsChange = 0;
        }

        if(_optionsChange == 0)
        {
            PositionInputs[0].transform.gameObject.SetActive(true);
            PositionInputs[1].transform.gameObject.SetActive(true);
            PositionInputs[2].transform.gameObject.SetActive(true);

            RotationInputs[0].transform.gameObject.SetActive(false);
            RotationInputs[1].transform.gameObject.SetActive(false);
            RotationInputs[2].transform.gameObject.SetActive(false);

            ScaleInputs[0].transform.gameObject.SetActive(false);
            ScaleInputs[1].transform.gameObject.SetActive(false);
            ScaleInputs[2].transform.gameObject.SetActive(false);
            ScaleInputs[3].transform.gameObject.SetActive(false);

            optionsText.text = "Position";
        }
        else if(_optionsChange == 1)
        {
            PositionInputs[0].transform.gameObject.SetActive(false);
            PositionInputs[1].transform.gameObject.SetActive(false);
            PositionInputs[2].transform.gameObject.SetActive(false);

            RotationInputs[0].transform.gameObject.SetActive(true);
            RotationInputs[1].transform.gameObject.SetActive(true);
            RotationInputs[2].transform.gameObject.SetActive(true);

            ScaleInputs[0].transform.gameObject.SetActive(false);
            ScaleInputs[1].transform.gameObject.SetActive(false);
            ScaleInputs[2].transform.gameObject.SetActive(false);
            ScaleInputs[3].transform.gameObject.SetActive(false);

            optionsText.text = "Rotation";
        }
        else if (_optionsChange == 2)
        {
            PositionInputs[0].transform.gameObject.SetActive(false);
            PositionInputs[1].transform.gameObject.SetActive(false);
            PositionInputs[2].transform.gameObject.SetActive(false);

            RotationInputs[0].transform.gameObject.SetActive(false);
            RotationInputs[1].transform.gameObject.SetActive(false);
            RotationInputs[2].transform.gameObject.SetActive(false);

            ScaleInputs[0].transform.gameObject.SetActive(true);
            ScaleInputs[1].transform.gameObject.SetActive(true);
            ScaleInputs[2].transform.gameObject.SetActive(true);
            ScaleInputs[3].transform.gameObject.SetActive(true);

            optionsText.text = "Scale";
        }
    }

    // Fonction de modification de la position, rotation et scale (Axes séparés) de l'objet
    public void ModifyValues()
    {
        // Changements de l'objet sélectionné en fonction des valeurs données
        for (int i = 0; i < allObjects.Length; i++)
        {
            if (isObjectUsed[i])
            {
                // POSITION
                allObjects[i].transform.position = new Vector3(float.Parse(PositionInputs[0].text), float.Parse(PositionInputs[1].text), float.Parse(PositionInputs[2].text));
                // ROTATION
                allObjects[i].transform.eulerAngles = new Vector3(float.Parse(RotationInputs[0].text), float.Parse(RotationInputs[1].text), float.Parse(RotationInputs[2].text));
                // SCALE
                allObjects[i].transform.localScale = new Vector3(float.Parse(ScaleInputs[0].text), float.Parse(ScaleInputs[1].text), float.Parse(ScaleInputs[2].text));
            }
        }
    }

    // Fonction de modification du Scale global de l'objet par multiplication ou division
    public void MultiplyScale()
    {
        for (int i = 0; i < allObjects.Length; i++)
        {
            if (isObjectUsed[i])
            {
                // Multiply
                if (toggleScale[0].isOn)
                {
                    // Changements du Scale en multipliant
                    allObjects[i].transform.localScale = new Vector3(float.Parse(ScaleInputs[0].text), float.Parse(ScaleInputs[1].text), float.Parse(ScaleInputs[2].text)) * float.Parse(ScaleInputs[3].text);
                }
                // Divide
                else if (toggleScale[1].isOn)
                {
                    // Changements du Scale en divisant
                    allObjects[i].transform.localScale = new Vector3(float.Parse(ScaleInputs[0].text), float.Parse(ScaleInputs[1].text), float.Parse(ScaleInputs[2].text)) / float.Parse(ScaleInputs[3].text);
                }

                // Affichage des nouvelles valeurs sur les zones d'inputs de chaque axe
                ScaleInputs[0].text = allObjects[i].transform.localScale.x.ToString();
                ScaleInputs[1].text = allObjects[i].transform.localScale.y.ToString();
                ScaleInputs[2].text = allObjects[i].transform.localScale.z.ToString();
            }
        }
    }
}
