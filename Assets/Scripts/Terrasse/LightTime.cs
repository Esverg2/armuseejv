using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class LightTime : MonoBehaviour
{

    //Directionnal light qui sert de soleil
    public Light sun;
    //Heure en temps réel en fonction de l'heure de l'appreil
    public float actualTime /*= System.DateTime.Now.Hour*/;
    //Date en fonction de la date de l'appareil
    public int actualDate = System.DateTime.Now.DayOfYear;
    //Période d'ensolleillement en fonciton de l'heure d'hiver ou d'été
    private int _dayHours;

    /*
    //Couleur du soleil en fonction de l'heure sous forme de dégradé -> a réglér dans l'éditeur
    public Gradient sunColor;*/

    //Plusieurs gradients en fonction de la météo, pour la couleur du soleil
    public Gradient[] weathers;
    //Définis quel gradient on utilise.
    public int id = 0;

    //180° levée de soleil
    //0° Couchée de soleil
    //90° Midi

    //utiliser l'api
    string API_key = "b87af28b2efaec5e27e959e9bb797570";
    //Lattitude et longitude
    float l = 0, L = 0;

    //Le code de la météo
    int meteo = -1;

    /*
     * 8xx = nuages
     * 7xx = brouillard
     * 6xx = neige
     * 5xx = pluie
     * 3xx = drizzle
     * 2xx = orage
     * */

    void Start()
    {
        actualDate = System.DateTime.Now.DayOfYear;

        //détecte si au jour où est réglé l'appareil nous sommes en heure d'hiver ou heure d'été, et modifie la période d'ensolleimment en fonction
        if (actualDate > 299 && actualDate < 367 || actualDate < 87)
        {
            _dayHours = 18;
        }
        if (actualDate > 87 && actualDate < 299)
        {
            _dayHours = 24;
        }

        //Lancer la coroutine pour récupérer les infos de l'api
        StartCoroutine(Started());
    }

    //Changer les données par la météo
    public void ChangeWeather()
    {

        if (meteo > 800)
        {
            //Changer l'id = changer le gradient.
            id = 7;
        }
        else if (meteo == 800)
        {
            id = 6;
        }
        else if (meteo >700)
        {
            id = 5;
        }
        else if (meteo > 600)
        {
            id = 4;
        }
        else if (meteo > 500)
        {
            id = 3;
        }
        else if (meteo > 300)
        {
            id = 2;
        }
        else if (meteo > 200)
        {
            id = 1;
        }
        else
        {
            id = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Actualise le temps en fonction de l heure de l'appareil
            actualTime = System.DateTime.Now.Hour;
            actualTime += (System.DateTime.Now.Minute / 60) / 10;
        //actualise l'angle de la lumière en fonction de l'heure
        sun.gameObject.transform.eulerAngles = new Vector3(actualTime * 180 / _dayHours, 180,0f);
        //Modifie la couleur de la lumière en fonction de l'heure
        sun.color = weathers[id].Evaluate(actualTime / 24);

        /*if (Input.GetKeyDown(KeyCode.E))
        {
            ChangeWeather(1);
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            ChangeWeather(0);
        }*/
    }

    IEnumerator Started()
    {

        Input.location.Start();
        // Vérifier si on a bien activé la localisation. Sinon mettre la météo par défaut.
        if (!Input.location.isEnabledByUser)
        {
            meteo = 0;
            ChangeWeather();
            yield break;
        }
            


        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {
            //On récupère la latitude et la longitude
            l = Input.location.lastData.latitude;
            L = Input.location.lastData.longitude;
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();

        //Une fois qu'on a la position, on passe à la météo
        StartCoroutine(GetWeather());
    }

    IEnumerator GetWeather()
    {
        //On récupère l'adresse en fonction de la position.
        string uri = "api.openweathermap.org/data/2.5/weather?" + "lat=" + l + "&lon=" + L + "&appid=" + API_key;

        //Requête pour récupérer les infos
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            yield return webRequest.SendWebRequest();
            if (webRequest.isNetworkError)
            {
                Debug.Log("Web request error: " + webRequest.error);
            }
            else
            {
                //Si c'est réussi, on parse le JSON obtenu
                GetData(webRequest.downloadHandler.text);
                //Puis on finit par activer la bonne météo.
                ChangeWeather();
            }
        }

        
    }

    //Sert à parse le JSON
    void GetData(string json)
    {
        //On le coupe plusieurs fois pour isoler l'information "id", qui indique le type de meteo. On fait ça manuellement parce que le json contient une donnée "base", qui rend la parse automatique impossible.
        string[] parse1 = json.Split('[');

        string[] parse2 = parse1[1].Split(',');

        string[] parseid1 = parse2[0].Split(':');

        meteo = int.Parse(parseid1[1]);

        string[] parseweather1 = parse2[2].Split(':');
    }
}
