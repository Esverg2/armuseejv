﻿using System.Collections;
using System.Collections.Generic;
//using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;

public class TellTime : MonoBehaviour
{
    public Text h, m;
    public Text currentWeatherText;
    string API_key= "b87af28b2efaec5e27e959e9bb797570";
    float l = 0, L = 0;

    int id=-1;
    string description;

    void Start()
    {
        float perc = System.DateTime.Now.Minute;
        perc /= 60;
        h.text = System.DateTime.Now.Hour.ToString();
        m.text = perc.ToString();

        m.transform.localPosition = Vector3.Lerp(new Vector3(0, 0, 0), new Vector3(500, 0, 0), perc);

        StartCoroutine(Started());

    }

    IEnumerator Started()
    {

        Debug.Log("a");
        Input.location.Start();
        // First, check if user has location service enabled
        if (Input.location.isEnabledByUser)
        {
            Debug.Log("potaot");
        }

        if (!Input.location.isEnabledByUser)
            yield break;

        Debug.Log("b");
        // Start service before querying location
        

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {

            l = Input.location.lastData.latitude;
            L = Input.location.lastData.longitude;

            h.text = l.ToString();
            m.text = L.ToString();
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();

        StartCoroutine(GetWeather());
    }

    IEnumerator GetWeather()
    {
        Debug.Log("c");
        string uri = "api.openweathermap.org/data/2.5/weather?"+ "lat=" + l + "&lon=" + L + "&appid=" + API_key;

        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            yield return webRequest.SendWebRequest();
            if (webRequest.isNetworkError)
            {
                Debug.Log("pa^dpaoid");
                Debug.Log("Web request error: " + webRequest.error);
            }
            else
            {
                GetData(webRequest.downloadHandler.text);

                h.text = description;
                m.text = id.ToString();
                
            }
        }

        Debug.Log("d");
    }

    void GetData(string json)
    {
        string[] parse1= json.Split('[');

        string[] parse2 = parse1[1].Split(',');

        string[] parseid1 = parse2[0].Split(':');

        id = int.Parse(parseid1[1]);

        string[] parseweather1 = parse2[2].Split(':');

        description = parseweather1[1];
        description = description.Remove(0, 1);
        description = description.Remove(description.Length - 1, 1);
    }

   
}
